
# Git Cheatsheet
Working with Git: 
```|WORK_DIR| <-> |STAGE| <-> |COMMIT|```

---
### Initialize local git repository 
```
git init
```
---

### Working with configs
```
git config --global --list
git config --local --list
```

```
git config --local user.name username
git config --local user.email email
```

```
git config --global user.name username
git config --global user.email email
```
---
### Status of current changes in working directory and in stage
```
git status
```
---

### Staging a file or multiple files for commit
```
git add filename 
git add .
```
---

### Commiting changes
```
git commit -m "commit msg"
git commit -am "commit msg" #stage all changes and commit

#Change commit of last commit
git commit --amend
```
---

### Working with logs
```
git log 
git log --oneline
```
---

### Looking at changes
```
git diff 
git diff --cached
```
---

# Resetting changes
```
#reseting staged changes
git reset HEAD file_name

#resetting unstages changes
git checkout -- <filename>

#reseting a branch
git checkout branch #discard all changes
```
---

### Branching
```
git branch

#create a branch
git branch <branch-name> 

#checkout a branch
git checkout <branch-name>
git checkout -b <name> #create a branch and change to it.

#rename/move a branch
git branch -m <old-name> <new-name>

#delete branch
git branch -d <branch-name>

#delete without merge
git branch -D <branch-name>
```
---

## Merging
```
git checkout <parent-branch>
git merge <branch-name> 
git merge <branch1> <branch2>...
```
---

### UI for git
```
gitk #UI for git
```
---

### Rebase commit from main/common/master branch to your branch
```
#rebase from a branch
git rebase <from-branch-name>

git rebase --continue
git rebase --skip
```
---

### Stashing: Saving uncommited changes using stash 
```
#Save uncomitted changes as into stash
git stash save

#List all stashes
git stash list

#Restore the Last saved stash into he current branch
git stash pop
```
---

### Working with reset
```
git reset --soft | --hard | --mixed <commit or pos wrt HEAD>

# --mixed is default option

git reset HEAD~pos
```
---

## Remote commands

### Cloning a repository
```
git clone <git-url>
```

### Pulling changes

```
git pull <remote> <branch>
```

### Pushing changes
```
git push <remote> <branch>
```

---
